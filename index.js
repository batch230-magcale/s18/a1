/*
	
	
	1.  [function WITHOUT return]
		Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function (test arguments: 5 and 15)
		-invoke and pass 2 arguments to the subtraction function (test arguments:  20 and 5)

	
	2.  [function with return]	
		Create a function which will be able to multiply two numbers. (test arguments: 50 and 10) 
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers. (test arguments: 50 and 10) 
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. [function with return]	
		Create a function which will be able to get total area of a circle from a radius number (not variable) as argument (test argument: 15) .
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive a result from an argument and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. [function with return]	
		Create a function which will be able to get total average of four numbers. (test arguments: 20, 40, 60, and 80)
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive arguments and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. [function with return]	
		Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/



// ------------------

function getSum(addend1, addend2){
	console.log("Displayed sum of " + addend1 + " and " + addend2 + ":");
	console.log(addend1 + addend2);
};

getSum(5, 15);

function getDifference(subtrahend, minuend){
	console.log("Displayed sum of " + minuend + " and " + subtrahend + ":");
	console.log(minuend + subtrahend);
};

getDifference(5, 15);



function getProduct(multiplier1, multiplier2){
	console.log("The product of " + multiplier1 + " and " + multiplier2 + ":");
	return multiplier1 * multiplier2;
}

let product = getProduct(50, 10);
console.log(product);


function getQuotient(dividend, divisor){
	console.log("The quotient of "+ dividend + " and " + divisor + ":");
	return dividend / divisor;
}

let quotient = getQuotient(50, 10);
console.log(quotient);


function getCircleArea(radius){
		return Math.PI * (radius ** 2);
}

let circleArea = getCircleArea(15);
console.log(Math.fround(circleArea));


function getAverage(ave1, ave2, ave3, ave4){
	console.log("The average of " + ave1 + ", " + ave2 + ", " + ave3 + ", " + ave4 + ":" );
	return (ave1 + ave2 + ave3 + ave4) / 4;
}

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);

function checkIfPass(score, total){
		let isPassed = ((score/total)*100) >= 75;
		console.log("Is " + score + "/" + total + " a passing score?");
		return isPassed;
	};

	let isPassingScore = checkIfPass(38, 50);
	console.log(isPassingScore);